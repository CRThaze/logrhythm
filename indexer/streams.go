package indexer

import (
	"bufio"
	"compress/bzip2"
	"compress/gzip"
	"container/list"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"github.com/h2non/filetype"
	fileTypes "github.com/h2non/filetype/matchers"
	"github.com/h2non/filetype/types"
)

type FileFormat types.Type

var (
	Gzip  = FileFormat(fileTypes.TypeGz)
	Bzip2 = FileFormat(fileTypes.TypeBz2)
	// XZ    = FileFormat(fileTypes.TypeXz)
	// LZMA  = FileFormat(fileTypes.TypeXz)
	// TAR = FileFormat(fileTypes.TypeTar)
	// AR    = FileFormat(fileTypes.TypeAr)
	// Seven = FileFormat(fileTypes.Type7z)
	// Z     = FileFormat(fileTypes.TypeZ)
	// Zip = FileFormat(fileTypes.TypeZip)
	// Lz  = FileFormat(fileTypes.TypeLz)
	// RAR   = FileFormat(fileTypes.TypeRar)
	TXT   = FileFormat(filetype.AddType("txt", "text/plain"))
	LOG   = FileFormat(filetype.AddType("log", "text/plain"))
	PLAIN = FileFormat(filetype.AddType("", "text/plain"))
)

const (
	// DefaultRotatedFileTimeLayout will be used if no Layout is specified for a log stream.
	DefaultRotatedFileTimeLayout = time.DateOnly
)

type LogFile interface {
	Path() string
	Time() time.Time
	Format() FileFormat
}

type logFile struct {
	path   string
	time   *time.Time
	format *FileFormat
}

type streamCursor struct {
	file       *list.Element
	fileHandle *os.File
	scanner    *bufio.Scanner
}

type LogStream interface {
	Name() string
	LogFilePath() string
	RotatedFilePattern() string
	RotatedFileTimeLayout() string
	RotatedFileDir() string
	RotatedFiles() []LogFile
	RotationFormat() FileFormat
	NextLog() ([]byte, *[2]*time.Time, error)
}

type logStream struct {
	name                  string
	logFile               *logFile
	logFormat             LogFormat
	rotatedFilePattern    string
	rotatedFileTimeLayout string
	rotatedFileDir        string
	streamFiles           *list.List
	rotationFormat        *FileFormat
	cursor                streamCursor
}

func (s *logStream) Name() string {
	return s.name
}

func (s *logStream) LogFilePath() string {
	return s.logFile.path
}

func (s *logStream) RotatedFilePattern() string {
	return s.rotatedFilePattern
}

func (s *logStream) RotatedFileTimeLayout() string {
	return s.rotatedFileTimeLayout
}

func (s *logStream) RotatedFileDir() string {
	return s.rotatedFileDir
}

func (s *logStream) RotatedFiles() []LogFile {
	paths := make([]LogFile, s.streamFiles.Len(), s.streamFiles.Len())

	i := 0
	for e := s.streamFiles.Front(); e != nil; e = e.Next() {
		paths[i] = e.Value.(LogFile)
		i++
	}
	return paths
}

func (s *logStream) RotationFormat() FileFormat {
	return *s.rotationFormat
}

func (s *logStream) NextLog() ([]byte, *[2]*time.Time, error) {
	if s.cursor.file == nil {
		// Base Case. There are no more files or lines to read.
		return nil, nil, nil
	}
	if s.cursor.scanner == nil {
		var err error
		s.cursor.fileHandle, err = os.Open(s.cursor.file.Value.(*logFile).path)
		if err != nil {
			return nil, nil, err
		}

		// Create the scanner, decompressing if necessary.
		switch *s.cursor.file.Value.(*logFile).format {
		case Bzip2:
			s.cursor.scanner = bufio.NewScanner(bzip2.NewReader(s.cursor.fileHandle))
		case Gzip:
			gzipReader, err := gzip.NewReader(s.cursor.fileHandle)
			if err != nil {
				return nil, nil, err
			}
			s.cursor.scanner = bufio.NewScanner(gzipReader)
		case LOG, TXT, PLAIN:
			fallthrough
		default:
			s.cursor.scanner = bufio.NewScanner(s.cursor.fileHandle)
		}

		s.cursor.scanner.Split(bufio.ScanLines)
	}

	// If there are no more lines to scan, advance the cursor to the next file.
	if !s.cursor.scanner.Scan() {
		s.cursor.fileHandle.Close()
		s.cursor.scanner = nil
		s.cursor.file = s.cursor.file.Next()
		return s.NextLog()
	}

	timeArray := [2]*time.Time{}
	line, realTime, err := ParseLogLine(s.cursor.scanner.Bytes(), s.logFormat)
	if realTime != nil {
		timeArray[0] = realTime
		timeArray[1] = realTime
	} else {
		// TODO: ensure this produces a good pseudo time.
		timeArray[0] = s.cursor.file.Value.(*logFile).time
	}

	return line, &timeArray, err
}

func (s logStream) findRotatedFiles() error {
	re, err := regexp.Compile(s.RotatedFilePattern())
	captureNames := re.SubexpNames()
	timestampCaptureIndex := -1
	for i, group := range captureNames {
		if group == "timestamp" {
			timestampCaptureIndex = i + 1
		}
	}

	if err != nil {
		return err
	}
	entries, err := os.ReadDir(s.RotatedFileDir())
	if err != nil {
		return err
	}
	for _, entry := range entries {
		if entry.IsDir() {
			continue
		}

		// Check if entry name matches rotated file pattern.
		matches := re.FindStringSubmatch(entry.Name())
		if len(matches) == 0 {
			continue
		}

		// See if we found a timestamp.
		var timestamp *time.Time
		if len(matches) > timestampCaptureIndex {
			ts, err := time.Parse(s.RotatedFileTimeLayout(), matches[timestampCaptureIndex])
			if err == nil {
				timestamp = &ts
			}
		}
		// If we didn't then get the file's mtime.
		if timestamp == nil {
			info, err := entry.Info()
			if err != nil {
				return err
			}
			mtime := info.ModTime()
			timestamp = &mtime
		}

		// Create a logFile object.
		currentLog := logFile{
			path:   filepath.Join(s.RotatedFileDir(), entry.Name()),
			time:   timestamp,
			format: s.rotationFormat,
		}

		// Insert the log file in the right location of the list.
		if s.streamFiles.Len() == 0 {
			s.streamFiles.PushFront(&currentLog)
		} else if s.streamFiles.Back().Value.(*logFile).time.Compare(*currentLog.time) > 0 {
			for e := s.streamFiles.Back(); true; e.Prev() {
				if e == nil {
					s.streamFiles.PushFront(&currentLog)
					break
				}
				if e.Value.(*logFile).time.Compare(*currentLog.time) < 1 {
					s.streamFiles.InsertAfter(&currentLog, e)
					break
				}
			}
		} else {
			s.streamFiles.PushBack(&currentLog)
		}
	}
	return nil
}

// NewLogStream is a constructor for LogStreams.
func NewLogStream(
	name,
	logFilePath,
	rotatedFilePattern,
	rotatedFileTimeLayout,
	rotatedFileDir string,
	rotationFormat *FileFormat,
	rotatedFileNames []string,
) (LogStream, error) {
	now := time.Now()

	s := logStream{
		name: name,
		logFile: &logFile{
			path: logFilePath,
			time: &now,
			// TODO: Detect logfile format.
			format: &PLAIN,
		},
		rotatedFilePattern:    rotatedFilePattern,
		rotatedFileTimeLayout: rotatedFileTimeLayout,
		rotatedFileDir:        rotatedFileDir,
		rotationFormat:        rotationFormat,
		streamFiles:           list.New(),
	}

	// Assume logs are rotated to the same directory if none is specified.
	if s.rotatedFileDir == "" {
		s.rotatedFileDir = filepath.Dir(s.LogFilePath())
	}

	// Assume "2006-01-02" layout if none is specified.
	if s.rotatedFileTimeLayout == "" {
		s.rotatedFileTimeLayout = time.DateOnly
	}

	// Use a default pattern for rotated log files if none is specified.
	if s.rotatedFilePattern == "" {
		// Log file name, minus extension, with the rest dedicated to the timestamp.
		s.rotatedFilePattern = fmt.Sprintf(
			"^%s(?P<timestamp>.*)$",
			strings.TrimSuffix(filepath.Base(s.LogFilePath()), filepath.Ext(s.LogFilePath())),
		)
	}

	// Find rotated log files if they were not explicitly specified.
	if len(rotatedFileNames) == 0 {
		if err := s.findRotatedFiles(); err != nil {
			return nil, err
		}
	} else {
		for _, name := range rotatedFileNames {
			s.streamFiles.PushFront(&logFile{
				path:   filepath.Join(s.RotatedFileDir(), name),
				format: s.rotationFormat,
			})
		}
	}

	s.streamFiles.PushBack(s.logFile)

	s.cursor = streamCursor{
		file: s.streamFiles.Front(),
	}

	return &s, nil
}
