package indexer

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	RawLogFieldName = "_raw"
)

type LogParseFunc func([]byte) (map[string]interface{}, error)

var (
	JSONParseFunc LogParseFunc = func(log []byte) (map[string]interface{}, error) {
		result := map[string]interface{}{}
		result[RawLogFieldName] = log
		return result, json.Unmarshal(log, &result)
	}
	// TODO: NCSA Common Log Format
	// TODO: Syslog Format
)

func NewRegexParseFunc(regexPattern string) (LogParseFunc, error) {
	re, err := regexp.Compile(regexPattern)
	if err != nil {
		return nil, err
	}
	namedCaptureGroups := re.SubexpNames()
	return func(log []byte) (map[string]interface{}, error) {
		result := map[string]interface{}{}
		result[RawLogFieldName] = log
		matches := re.FindSubmatch(log)
		for i, name := range namedCaptureGroups {
			if name == "" {
				continue
			}
			result[name] = matches[i]
		}
		return result, nil
	}, nil
}

type LogFormat struct {
	TimestampField  string
	TimestampIsUnix bool
	UnixUnit        time.Duration
	TimestampLayout string
	ParseFunc       LogParseFunc
}

func unixFieldToTime(timestampField interface{}, unit time.Duration) (*time.Time, error) {
	var timestamp time.Time
	var err error
	var t int

	// Reusable block to convert a unix timestamp string.
	stringTimeToInt := func(tStr string) (int, error) {
		// If there's a decimal in the string, split on it.
		portions := strings.Split(tStr, ".")
		// If there was a single decimal point determine the precision of the decmial, and update the unit.
		// Otherwise it's invalid.
		// TODO: handle decimals when the unit is not seconds.
		if len(portions) == 2 {
			switch len(portions[1]) {
			case 1, 2, 3:
				unit = time.Millisecond
			case 4, 5, 6:
				unit = time.Microsecond
			case 7, 8, 9:
				unit = time.Nanosecond
			}
		} else if len(portions) > 2 {
			return 0, fmt.Errorf("Invalid timestamp (%s)", tStr)
		}
		// Promote decimal string to integer and convert.
		return strconv.Atoi(strings.Join(portions, ""))
	}

	// Swtich on the type of the timestampField to convert to int.
	switch timestampField.(type) {
	case []byte:
		t, err = stringTimeToInt(string(timestampField.([]byte)))
		if err != nil {
			return nil, err
		}
	case string:
		t, err = stringTimeToInt(timestampField.(string))
		if err != nil {
			return nil, err
		}
	case int:
		t = int(timestampField.(int))
	case int8:
		t = int(timestampField.(int8))
	case int16:
		t = int(timestampField.(int16))
	case int32:
		t = int(timestampField.(int32))
	case int64:
		t = int(timestampField.(int64))
	case uint:
		t = int(timestampField.(uint))
	case uint8:
		t = int(timestampField.(uint8))
	case uint16:
		t = int(timestampField.(uint16))
	case uint32:
		t = int(timestampField.(uint32))
	case uint64:
		t = int(timestampField.(uint64))
	case float32:
		t = int(timestampField.(float32))
	case float64:
		t = int(timestampField.(float64))
	}

	// Switch on the timestamp unit to convert to a time.Time struct.
	switch unit {
	case time.Second:
		timestamp = time.Unix(int64(t), 0)
	case time.Millisecond:
		timestamp = time.UnixMilli(int64(t))
	case time.Microsecond:
		timestamp = time.UnixMicro(int64(t))
	case time.Nanosecond:
		timestamp = time.Unix(int64(t/1000000000), int64(t-(t/1000000000)))
	}
	return &timestamp, nil
}

func ParseLogLine(log []byte, format LogFormat) ([]byte, *time.Time, error) {
	// Use the format's parse func to parse the raw log line.
	parsed, err := format.ParseFunc(log)
	if err != nil {
		// If we had any error parsing, return the unparsed log with an error.
		return log, nil, err
	}

	// Figure out the timestamp if possible.
	var timestamp *time.Time
	if timestampField, ok := parsed[format.TimestampField]; ok {
		if format.TimestampIsUnix {
			timestamp, err = unixFieldToTime(timestampField, format.UnixUnit)
		}
		if format.TimestampLayout != "" {
			var t time.Time
			t, err = time.Parse(format.TimestampLayout, timestampField.(string))
			if err == nil {
				timestamp = &t
			}
		}
	}

	// Encode the log line back as bytes in JSON format.
	log, err = json.Marshal(parsed)
	if err != nil {
		return nil, nil, err
	}
	return log, timestamp, err
}
