package indexer

import (
	"container/list"
	"fmt"
	"time"

	"github.com/emirpasic/gods/maps/treemap"
	gods "github.com/emirpasic/gods/utils"
)

// type logSet []Log

// func (s logSet) Data() ([]byte, error) {
// 	logSlice := make([]*Log, len(s), len(s))
// 	i := 0
// 	for _, v := range s {
// 		v.loadLine = true
// 		defer func() {
// 			v.loadLine = false
// 		}()
// 		logSlice[i] = &v
// 		i++
// 	}
// 	return json.Marshal(logSlice)
// }

type LogMetadata struct {
	PseudoTimestamp time.Time
	RealTimestamp   *time.Time
	Stream          string
	FileName        string
	streamFile      string
	streamFileLine  uint
}

type Log struct {
	Meta LogMetadata `json:"meta"`
	Line interface{} `json:"line"`
}

type logSet struct {
	treemap.Map
	count int
}

func (s *logSet) insert(log *Log) {
	if l, ok := s.Get(log.Meta.PseudoTimestamp); ok {
		l.(*list.List).PushBack(log)
	} else {
		l := list.New()
		l.PushBack(log)
		s.Put(log.Meta.PseudoTimestamp, l)
	}
	s.count++
}

func (s *logSet) toSlice() []*Log {
	result := make([]*Log, s.count, s.count)
	i := 0
	it := s.Iterator()
	for it.Next() {
		for e := it.Value().(*list.List).Front(); e != nil; e = e.Next() {
			result[i] = e.Value.(*Log)
		}
	}
	return result
}

func (s *logSet) trim(from time.Time, before bool) {
	min, _ := s.Min()
	max, _ := s.Max()
	mid := ((max.(int64) - min.(int64)) / 2) + min.(int64)

	it := s.Iterator()

	// Determine if it will more likely be faster to start from the end.
	// This assumes that the distribution of indexes is even.
	if mid < from.Unix() {
		it.End()
		it.PrevTo(func(key, value interface{}) bool {
			if key.(int64) < from.Unix() {
				return true
			}
			return false
		})
		if before {
			s.Remove(it.Key())
			for it.Prev() {
				s.Remove(it.Key())
			}
			return
		}
		for it.Next() {
			s.Remove(it.Key())
		}
		return
	}

	it.NextTo(func(key, value interface{}) bool {
		if key.(int64) > from.Unix() {
			return true
		}
		return false
	})
	if before {
		for it.Prev() {
			s.Remove(it.Key())
		}
		return
	}
	s.Remove(it.Key())
	for it.Next() {
		s.Remove(it.Key())
	}
}

func mergeLogSets(toMerge ...*logSet) *logSet {
	merged := newLogSet()
	for _, set := range toMerge {
		set.Each(func(key, value interface{}) {
			if l, ok := merged.Get(key); ok {
				l.(*list.List).PushBackList(value.(*list.List))
				return
			}
			merged.Put(key, value)
		})
	}
	return merged
}

func newLogSet() *logSet {
	tm := treemap.NewWith(gods.Int64Comparator)
	return &logSet{Map: *tm}
}

type indexBucket struct {
	startTime time.Time
	size      time.Duration
	logs      *logSet
	// filePath  string
}

func (b *indexBucket) endTime() time.Time {
	return b.startTime.Add(b.size)
}

func (b *indexBucket) insert(log *Log) error {
	if log.Meta.PseudoTimestamp.Compare(b.endTime()) > 0 || log.Meta.PseudoTimestamp.Compare(b.startTime) < 0 {
		return fmt.Errorf("Log's timestamp (%v) is outside of bucket's range", log.Meta.PseudoTimestamp)
	}
	if b.logs == nil {
		b.logs = newLogSet()
	}
	b.logs.insert(log)
	return nil
}

type index struct {
	baseTime   time.Time
	bucketSize time.Duration
	buckets    []*indexBucket
}

func (i *index) findBucketIndex(timestamp time.Time) int {
	fromBase := i.baseTime.Sub(timestamp)
	if fromBase < 0 {
		return -1
	}
	return int(fromBase / i.bucketSize)
}

func (i *index) insert(log *Log) error {
	bi := i.findBucketIndex(log.Meta.PseudoTimestamp)
	if bi < 0 {
		return fmt.Errorf("Log's timestamp (%v) is after index's base time", log.Meta.PseudoTimestamp)
	}

	if bi >= cap(i.buckets) {
		ns := make([]*indexBucket, bi+1)
		copy(ns, i.buckets)
		i.buckets = ns
	} else if bi > len(i.buckets) {
		i.buckets = i.buckets[:bi+1]
	}

	if i.buckets[bi] == nil {
		i.buckets[bi] = &indexBucket{
			// TODO: init bucket
		}
	}

	i.buckets[bi].insert(log)
	return nil
}

func (i *index) fetchLogs(from, till time.Time) *logSet {
	fromBucketIdx := i.findBucketIndex(from)
	tillBucketIdx := i.findBucketIndex(till)
	if fromBucketIdx < 0 || tillBucketIdx < 0 || fromBucketIdx < tillBucketIdx {
		return nil
	}
	if fromBucketIdx > len(i.buckets) {
		fromBucketIdx = len(i.buckets) - 1
	}
	// TODO: compact
	// TODO: get logsets
	// TODO: merge logsets
	// TODO: Return logsets
	return nil
}

func newIndex() *index {
	// tm := treemap.NewWith(gods.Int64Comparator)
	return &index{
		baseTime: time.Now(),
		buckets:  []*indexBucket{},
	}
}

var theIndex *index = newIndex()

// Index (take file streams to index)
func Index(streams []LogStream, bucketSize uint8) error {
	// TODO: Iterate over log streams.
	//
	return nil
}

// WriteBucket (write an indexed bucket
// LoadBucket (load bucket data from file / lazy load line)
// UnloadBucket (empty data)
// GetBucketsForRange (get slice of bucket pointers that cover a range of time)
// TrimLogSet Trim Logs before or after timestamp
// FetchLogs fetch a []byte of json encoded logs for a given range.
