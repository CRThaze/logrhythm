.DEFAULT_GOAL := all

#############
# Variables #
#############

EXECUTABLE := logs

ifdef GOOS
	EXECUTABLE := $(EXECUTABLE)-$(GOOS)
endif

ifdef GOARCH
	EXECUTABLE := $(EXECUTABLE)-$(GOARCH)
endif

VERSION ?= $(shell ./.version/calculate-version.sh)

LIBJQ_ARCHIVE := libjq-glibc-amd64.tgz
LIBJQ_ARCHIVE_URL := https://github.com/flant/libjq-go/releases/download/jq-b6be13d5-0/$(LIBJQ_ARCHIVE)
LIBJQ_PREFIX := libjq

###########
# TARGETS #
###########

$(LIBJQ_PREFIX):
	wget $(LIBJQ_ARCHIVE_URL)
	tar xzf $(LIBJQ_ARCHIVE)
	rm -f $(LIBJQ_ARCHIVE)

$(EXECUTABLE): $(LIBJQ_PREFIX)
	CGO_ENABLED=1 CGO_CFLAGS="-I$(PWD)/libjq/include" CGO_LDFLAGS="-L$(PWD)/libjq/lib" go build -ldflags="-X main.Version=$(VERSION)" -o $(EXECUTABLE) .

.PHONY: clean-build
clean-build:
	rm -f $(EXECUTABLE)

.PHONY: build
build: clean-build $(EXECUTABLE)

.PHONY: clean
clean: clean-build
	rm -rf $(LIBJQ_PREFIX)

.PHONY: all
all: build
